/* ************************************************************************** */
/** common.h

  @Author
    Galen Nare

  @File Name
    common.h

  @Summary
     Common header for project 4

     You may not distribute or modify this code without explicit, written permission from its author.
 */
/* ************************************************************************** */

#ifndef _COMMON_HEADER    /* Guard against multiple inclusion */
#define _COMMON_HEADER

    /*------------------ Board system settings. PLEASE DO NOT MODIFY THIS PART ----------*/
    #pragma config FPLLIDIV = DIV_2         // PLL Input Divider (2x Divider)
    #pragma config FPLLMUL = MUL_20         // PLL Multiplier (20x Multiplier)
    #pragma config FPLLODIV = DIV_1         // System PLL Output Clock Divider (PLL Divide by 1)
    #pragma config FNOSC = PRIPLL           // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
    #pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
    #pragma config POSCMOD = XT             // Primary Oscillator Configuration (XT osc mode)
    #pragma config FPBDIV = DIV_8           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/8)
    /*----------------------------------------------------------------------------*/
    
    #include <stdint.h>
    #include <xc.h>     // Microchip XC processor header which links to the PIC32MX370512L header
    #include <sys/attribs.h> 

    #include "adc.h"
    #include "lcd.h"
    #include "songs.h"
    #include "config.h" // Basys MX3 configuration header
    

    #define SYS_FREQ (80000000L) // 80MHz system clock
    #define MS_CONSTANT 1426

    void delay_ms(int);        // Function prototypes
    
    void CN_button_config();
    void CN_btn_press(unsigned int);
    
    void CN_keypad_config();
    void CN_key_press(unsigned int);
    
    void lcd_clear();
    void lcd_mode(unsigned int);
    
    void play_tone(unsigned int);
    
    void Timer1_Setup();
    void Timer2_Setup();
    
#endif /* _COMMON_HEADER */

#ifndef _SUPPRESS_PLIB_WARNING          // suppress the plib warning during compiling
        #define _SUPPRESS_PLIB_WARNING      
#endif

#ifndef _KEYPAD_CONSTANTS
#define _KEYPAD_CONSTANTS // Keypad and button location macros
    
    #define BTNU PORTBbits.RB1
    #define BTNL PORTBbits.RB0
    #define BTNC PORTFbits.RF0
    #define BTNR PORTBbits.RB8
    #define BTND PORTAbits.RA15
    
    #define R4 LATCbits.LATC14
    #define R3 LATDbits.LATD0
    #define R2 LATDbits.LATD1
    #define R1 LATCbits.LATC13

    #define C4 PORTDbits.RD9
    #define C3 PORTDbits.RD11
    #define C2 PORTDbits.RD10
    #define C1 PORTDbits.RD8

#endif
    
/* *****************************************************************************
 End of File
 */
