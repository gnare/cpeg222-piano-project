/* ************************************************************************** */
/**

  @Author
    Galen Nare

  @File Name
    project4main.c

  @Summary
    Main source file and control flow for CPEG 222 Piano project.

    You may not distribute or modify this code without explicit, written permission from its author.
 */
/* ************************************************************************** */

#include "common.h"

int mode = 1;

int song_idx = 0;
int note_idx = 0;
int song_delta_t = 0;
int song_flag = 0;

int pr2_last = 0;
int pr2_current = 0;
int note_dt = 0;
int note_duration = 0;

int main() {
    DDPCONbits.JTAGEN = 0; // Statement is required to use Pin RA0 as IO
    TRISA &= 0xFF00; // Set Port A bits 0~7 to 0, i.e., LD 0~7 are configured as digital output pins
    TRISFbits.TRISF0 = 1; // Set RF0 to 1, i.e., BTNC is configured as input  
    LATA = 0;
    
    TRISDbits.TRISD0 = 0; // Set PMOD port states for keypad
    TRISDbits.TRISD1 = 0;
    ANSELDbits.ANSD1 = 0;
    
    TRISCbits.TRISC14 = 0;
    TRISCbits.TRISC13 = 0;
    TRISDbits.TRISD8 = 1;
    TRISDbits.TRISD9 = 1;
    TRISDbits.TRISD10 = 1;
    TRISDbits.TRISD11 = 1;
    
    // Button Config
    TRISFbits.TRISF0 = 1;
    TRISBbits.TRISB1 = 1; // RB1 (BTNU) configured as input
    ANSELBbits.ANSB1 = 0; // RB1 (BTNU) disabled analog
    TRISBbits.TRISB0 = 1; // RB1 (BTNL) configured as input
    ANSELBbits.ANSB0 = 0; // RB1 (BTNL) disabled analog
    TRISBbits.TRISB8 = 1; // RB8 (BTNR) configured as input
    ANSELBbits.ANSB8 = 0; // RB8 (BTNR) disabled analog
    TRISAbits.TRISA15 = 1; // RA15 (BTND) configured as input

    TRISBbits.TRISB14 = 0; // Set states for OC1 module
    ANSELBbits.ANSB14 = 0;
    RPB14R = 0xC; // Set speaker output to OC1
    
    TRISBbits.TRISB4 = 1; // Set microphone to input
    ANSELBbits.ANSB4 = 1; // Set mic as analog
    
    init_songs();

    CN_button_config(); // Setup interrupts
    CN_keypad_config();
    Timer1_Setup();
    Timer2_Setup();
    
    LCD_Init(); // Setup I/O
    ADC_Init();
    
    lcd_clear();
    lcd_mode(mode);

    while (1) {
        delay_ms(10);
        if (mode == 2) {
            int mic_sample = ADC_AnalogRead(4); // For use in skipping songs
            if (mic_sample >= 576) {
                song_idx += 1;
                note_idx = 0;
                song_delta_t = 0;
                song_flag = 0;
                LATA = song_idx + 1;
                if (song_idx >= len_songs) {
                    mode = 1;
                    play_tone(0);
                    lcd_mode(mode);
                }
                if (song_idx < len_songs) {
                    lcd_show_song(songs[song_idx]);
                }
            }
        } else {
            LATA = 0; // No LEDs unless in mode 2
        }
    }; // Keep the program from exiting.
}

/* ----------------------------------------------------------------------------- 
 **	Timer1_Setup
 **	Parameters:
 **		none
 **	Return Value:
 **		none
 **	Description:
 **		Sets up Timer1 interrupts.
 ** -------------------------------------------------------------------------- */
void Timer1_Setup() {
    T1CON = 0x8000;
    PR1 = 5000;
    TMR1 = 0;
    
    IPC1bits.T1IP = 7;
    IPC1bits.T1IS = 2;
    IFS0bits.T1IF = 0;
    IEC0bits.T1IE = 1;
}

/* ----------------------------------------------------------------------------- 
 **	Timer2_Setup
 **	Parameters:
 **		none
 **	Return Value:
 **		none
 **	Description:
 **		Sets up Timer2 interrupts.
 ** -------------------------------------------------------------------------- */
void Timer2_Setup() {
    T2CON |= 0x78;
    T2CONbits.ON = 1;
    T2CONbits.TCKPS1 = 1;
    T2CONbits.TCKPS0 = 0;
    PR2 = 0; // Disable

    // OC1 pin/reg configuration
    OC1CONbits.ON = 0;
    OC1CONbits.OCM = 6;
    OC1CONbits.OCTSEL = 0;
    OC1RS = 0;
    OC1R = 0;
    OC1CONbits.ON = 1;
    
    IPC2bits.T2IP = 7;
    IPC2bits.T2IS = 3;
    IFS0bits.T2IF = 0;
    IEC0bits.T2IE = 1;
    macro_enable_interrupts(); // Commit the interrupt changes
}

/* ----------------------------------------------------------------------------- 
 **	CN_button_config
 **	Parameters:
 **		none
 **	Return Value:
 **		none
 **	Description:
 **		Sets up CN interrupts for the onboard buttons.
 ** -------------------------------------------------------------------------- */
void CN_button_config() {
    macro_disable_interrupts;
    
    CNCONBbits.ON = 1;
    CNCONAbits.ON = 1;
    
    CNENBbits.CNIEB0 = 1;
    CNENBbits.CNIEB1 = 1;
    CNENBbits.CNIEB8 = 1;
    CNENAbits.CNIEA15 = 1;
    
    CNPUBbits.CNPUB0 = 0;
    CNPUBbits.CNPUB1 = 0;
    CNPUBbits.CNPUB8 = 0;
    CNPUAbits.CNPUA15 = 0;
    
    PORTB;
    BTND;
    IPC8bits.CNIP = 6;
    IPC8bits.CNIS = 3;
    
    IFS1bits.CNBIF = 0;
    IEC1bits.CNBIE = 1;
    IFS1bits.CNAIF = 0;
    IEC1bits.CNAIE = 1;
    
    macro_enable_interrupts();
}

/* ----------------------------------------------------------------------------- 
 **	CN_keypad_config
 **	Parameters:
 **		none
 **	Return Value:
 **		none
 **	Description:
 **		Sets up CN interrupts for the keypad PMOD.
 ** -------------------------------------------------------------------------- */
void CN_keypad_config() {
    macro_disable_interrupts;
    
    CNCONDbits.ON = 1;
    CNEND = 0xf00;
    CNPUD = 0xf00;
    
    PORTD;
    IPC8bits.CNIP = 6;
    IPC8bits.CNIS = 3;
    IFS1bits.CNDIF = 0;
    IEC1bits.CNDIE = 1;
    
    macro_enable_interrupts();
}

/* ----------------------------------------------------------------------------- 
 **	CN_Handler
 **	Parameters:
 **		none
 **	Return Value:
 **		none
 **	Description:
 **		Handles all change notice interrupts and dispatches them to the correct
 **     helper function. This handles both onboard and keypad presses.
 ** -------------------------------------------------------------------------- */
void __ISR(_CHANGE_NOTICE_VECTOR) CN_Handler() { // CN ISR
    IEC1bits.CNDIE = 0; // Disable interrupt
    IEC1bits.CNBIE = 0;
    IEC1bits.CNAIE = 0;
    
    //LATA ^= 32; // DEBUG LED
    
    // Decode button press
    int key = 255;
    int button = 255;
    
    if (BTNU == 1) button = 0;
    else if (BTNL == 1) button = 1;
    else if (BTNC == 1) button = 2;
    else if (BTNR == 1) button = 3;
    else if (BTND == 1) button = 4;
    
    R1 = 0;
    R2 = R3 = R4 = 1;
    if (C1 == 0) key = 0x1;
    else if (C2 == 0) key = 0x2;
    else if (C3 == 0) key = 0x3;
    else if (C4 == 0) key = 0xa;
    
    R2 = 0;
    R1 = R3 = R4 = 1;
    if (C1 == 0) key = 0x4;
    else if (C2 == 0) key = 0x5;
    else if (C3 == 0) key = 0x6;
    else if (C4 == 0) key = 0xb;
    
    R3 = 0;
    R1 = R2 = R4 = 1;
    if (C1 == 0) key = 0x7;
    else if (C2 == 0) key = 0x8;
    else if (C3 == 0) key = 0x9;
    else if (C4 == 0) key = 0xc;
    
    R4 = 0;
    R1 = R2 = R3 = 1;
    if (C1 == 0) key = 0x0;
    else if (C2 == 0) key = 0xf;
    else if (C3 == 0) key = 0xe;
    else if (C4 == 0) key = 0xd;
    
    R1 = R2 = R3 = R4 = 0; // Clear all rows
    
    delay_ms(20); // Debounce
    CN_key_press(key); // Pass decoded button press to handler
    CN_btn_press(button);
    
    PORTD; // Wipe ports
    PORTB;
    BTND;
    
    IFS1bits.CNDIF = 0; // Clear interrupt flags
    IFS1bits.CNBIF = 0;
    IFS1bits.CNAIF = 0;
    IEC1bits.CNDIE = 1; // Re-enable interrupt
    IEC1bits.CNBIE = 1;
    IEC1bits.CNAIE = 1;
}

/* ----------------------------------------------------------------------------- 
 **	play_tone
 **	Parameters:
 **		pr_val - Value to set the period register to (determines tone frequency)
 **              Note: 355 -> 440Hz
 **	Return Value:
 **		none
 **	Description:
 **		Resets Timer2 and sets the PR2 register. Also assigns a state variable
 **     for use in mode 4.
 ** -------------------------------------------------------------------------- */
void play_tone(unsigned int pr_val) {
    IEC0bits.T2IE = 0;
    T2CONbits.ON = 0;
    TMR2 = 0;

    T2CON |= 0x78;
    T2CONbits.ON = 1;
    T2CONbits.TCKPS1 = 1;
    T2CONbits.TCKPS0 = 0;

    PR2 = pr_val;
    pr2_current = pr_val;

    IFS0bits.T2IF = 0;
    IEC0bits.T2IE = 1;

    OC1RS = PR2 / 2;
    OC1R = OC1RS;
}

/* ----------------------------------------------------------------------------- 
 **	CN_key_press
 **	Parameters:
 **		button - hex value of the key pressed (key label matches hex)
 **	Return Value:
 **		none
 **	Description:
 **		Handles keypad PMOD key press.
 ** -------------------------------------------------------------------------- */
void CN_key_press(unsigned int button) {
    if (button != 0x6 && button != 0xb && button != 0xd && button < 16) {
        
        if (mode == 3 || mode == 4) {
            play_tone(pr2_preset5_notes[button]);
        }
        
    } else if (button == 0xd) {
        mode = 1;
        lcd_mode(mode);
    } else {
        play_tone(0);
    }
}

/* ----------------------------------------------------------------------------- 
 **	CN_btn_press
 **	Parameters:
 **		button - the numeric value of the button (0-up;1-left;2-center;3-right;4-down)
 **	Return Value:
 **		none
 **	Description:
 **		Handle onboard button press.
 ** -------------------------------------------------------------------------- */
void CN_btn_press(unsigned int button) {
    if (button != 2 && button < 5) {
        if (mode == 1) {
            switch (button) {
                case 0:
                    mode = 4;
                    song_idx = 0;
                    note_idx = 0;
                    song_delta_t = 0;
                    song_flag = 0;
                    pr2_last = 0;
                    note_dt = 0;
                    note_duration = 0;
                    break;
                case 1:
                    note_idx = 0;
                    song_idx = 0;
                    song_delta_t = 0;
                    song_flag = 0;
                    mode = 2;
                    break;
                case 3:
                    mode = 3;
                    break;
                case 4:
                    mode = 5;
                    song_idx = 0;
                    note_idx = 0;
                    song_delta_t = 0;
                    song_flag = 0;
                    pr2_last = 0;
                    note_dt = 0;
                    note_duration = 0;
                    lcd_show_song(recorded_song);
                    break;
            }
            lcd_mode(mode);
        }
    } else {
        
    }
}

/* ----------------------------------------------------------------------------- 
 **	lcd_clear
 **	Parameters:
 **		none
 **	Return Value:
 **		none
 **	Description:
 **		Clears the LCD (library function appears to be bugged).
 ** -------------------------------------------------------------------------- */
void lcd_clear() {
    LCD_WriteStringAtPos("                ", 0, 0);
    LCD_WriteStringAtPos("                ", 1, 0);
}

/* ----------------------------------------------------------------------------- 
 **	lcd_show_song
 **	Parameters:
 **		song - current Song object playing
 **	Return Value:
 **		none
 **	Description:
 **		Display title and artist of song on LCD, then mode 2 text.
 **     Also delays the main loop to prevent inadvertently skipping more than 1 song.
 ** -------------------------------------------------------------------------- */
void lcd_show_song(Song* s) {
    lcd_clear();
    LCD_WriteStringAtPos(s->title, 0, 0); // NOTE comment out these lines to disable meta
    LCD_WriteStringAtPos(s->artist, 1, 0);
    delay_ms(1000);
    
    if (mode == 2) {
        lcd_clear();
        LCD_WriteStringAtPos("Clap to switch", 0, 0);
        LCD_WriteStringAtPos("Keypad D to end", 1, 0);
    } else if (mode == 5) {
        LCD_WriteStringAtPos("Playing recorded", 0, 0);
        LCD_WriteStringAtPos("Keypad D to stop", 1, 0);
    }
}

/* ----------------------------------------------------------------------------- 
 **	lcd_mode
 **	Parameters:
 **		mode - the current mode of the program
 **	Return Value:
 **		none
 **	Description:
 **		Set the LCD to the corresponding text for the mode.
 ** -------------------------------------------------------------------------- */
void lcd_mode(unsigned int mode) {
    lcd_clear();
    switch (mode) {
        case 1:
            LCD_WriteStringAtPos("8-Bit Music Box", 0, 0);
            LCD_WriteStringAtPos("Press BtnL/R/U/D", 1, 0);
            break;
        case 2:
            lcd_show_song(songs[0]);
            break;
        case 3:
            LCD_WriteStringAtPos("Keypad Piano", 0, 0);
            LCD_WriteStringAtPos("Keypad D to quit", 1, 0);
            break;
        case 4:
            LCD_WriteStringAtPos("REC & Piano", 0, 0);
            LCD_WriteStringAtPos("Keypad D to end", 1, 0);
            break;
        case 5:
            LCD_WriteStringAtPos("Playing recorded", 0, 0);
            LCD_WriteStringAtPos("Keypad D to stop", 1, 0);
            break;
    }
}

/* ----------------------------------------------------------------------------- 
 **	TMR1_Handler
 **	Parameters:
 **		none
 **	Return Value:
 **		none
 **	Description:
 **		Handle Timer1 Interrupt (for timing the playback of recorded notes)
 ** -------------------------------------------------------------------------- */
void __ISR(_TIMER_1_VECTOR) TMR1_Handler() {
    if (mode == 2) {
        if (note_idx >= songs[song_idx]->c_notes) {
            song_idx += 1;
            play_tone(0);
            note_idx = 0;
            song_delta_t = 0;
            song_flag = 0;
            if (song_idx < len_songs) {
                lcd_show_song(songs[song_idx]);
            }
        }

        if (song_idx >= len_songs) {
            mode = 1;
            lcd_mode(mode);
        } else {
            song_delta_t += 1;
            if (song_flag == 0) {
                if (song_delta_t >= songs[song_idx]->note_delta_t[note_idx]) {
                    play_tone(songs[song_idx]->note_pr[note_idx]);
                    song_delta_t = 0;
                    song_flag = 1;
                }
            } else {
                if (song_delta_t >= songs[song_idx]->note_duration[note_idx]) {
                    play_tone(0);
                    song_delta_t = 0;
                    song_flag = 0;
                    note_idx += 1;
                }
            }
        }
    } else if (mode == 4) {
        if (note_idx < 50) {
            if (pr2_last == pr2_current) {
                if (pr2_current > 0) {
                    note_duration += 1;
                    LATA ^= 8;
                } else {
                    note_dt += 1;
                    LATA ^= 16;
                }
            } else {
                if (pr2_current == 0) {
                    recorded_song->note_pr[note_idx] = pr2_last;
                    recorded_song->note_duration[note_idx] = note_duration;
                    recorded_song->note_delta_t[note_idx] = note_dt;
                    note_dt = 0;
                    note_duration = 0;
                    note_idx += 1;
                    LATA ^= 32;
                }
            }
        }
        pr2_last = pr2_current;
    } else if (mode == 5) {
        if (note_idx < 50) {
            song_delta_t += 1;
            if (song_flag == 0) {
                if (song_delta_t >= recorded_song->note_delta_t[note_idx]) {
                    play_tone(recorded_song->note_pr[note_idx]);
                    song_delta_t = 0;
                    song_flag = 1;
                }
            } else {
                if (song_delta_t >= recorded_song->note_duration[note_idx]) {
                    play_tone(0);
                    song_delta_t = 0;
                    song_flag = 0;
                    note_idx += 1;
                }
            }
        } else {
            play_tone(0);
            note_idx = 0;
            song_delta_t = 0;
            song_flag = 0;
            song_idx = 0;
            mode = 1;
            lcd_mode(mode);
        }
    }
    
    IFS0bits.T1IF = 0;
}


/* ----------------------------------------------------------------------------- 
 **	TMR2_Handler
 **	Parameters:
 **		none
 **	Return Value:
 **		none
 **	Description:
 **		Handle Timer2 Interrupt (for timing of audio samples)
 ** -------------------------------------------------------------------------- */
void __ISR(_TIMER_2_VECTOR) TMR2_Handler() { // Timer2 ISR
    /*if (i > 99) {
        i = 0; // Loop through array
    }
    
    // Assume sinArray is predefined 100 point sine waveform
    if (gen_tone) {
        OC1RS = PR2 / 2;
        OC1R = OC1RS;
    }

    i++;*/
    IFS0bits.T2IF = 0;
}

/* ----------------------------------------------------------------------------- 
 **	delay_ms
 **	Parameters:
 **		ms - amount of milliseconds to delay (based on 80 MHz SSCLK)
 **	Return Value:
 **		none
 **	Description:
 **		Create a delay by counting up to counter variable
 ** -------------------------------------------------------------------------- */
void delay_ms(int ms) {
    int i, counter;
    for (counter = 0; counter < ms; counter++) {
        for (i = 0; i < MS_CONSTANT; i++) {
        } //software delay 1 milliseconds
    }
}
