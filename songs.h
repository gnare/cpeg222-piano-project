/* ************************************************************************** */
/** songs.h

  @Author
    Galen Nare

  @File Name
    songs.h

  @Summary
    Custom songs header for project 4. Includes custom Song type definition for
    use in song playback w/ metadata.

    You may not distribute or modify this code without explicit, written permission from its author.
 */
/* ************************************************************************** */

#ifndef _SONGS_HEADER
#define _SONGS_HEADER
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>

    // NOTE; 8192 BYTES ALLOCATED TO HEAP

    // Usable notes in songs
    const int pr2_midi_preset[120] = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9553, 9016, 8510, 8033, 7582, 7156, 
        6755, 6376, 6018, 5680, 5361, 5060, 4776, 4508, 4255, 4016, 3791, 3578, 
        3377, 3188, 3009, 2840, 2681, 2530, 2388, 2254, 2128, 2008, 1895, 1789, 
        1689, 1594, 1504, 1420, 1340, 1265, 1194, 1127, 1064, 1004, 948, 895, 844, 
        797, 752, 710, 670, 633, 597, 564, 532, 502, 474, 447, 422, 398, 376, 355, 
        335, 316, 299, 282, 266, 251, 237, 224, 211, 199, 188, 178, 168, 158, 149, 
        141, 133, 126, 118, 112, 106, 100, 94, 89, 84, 79, 75, 70, 66, 63, 59, 56, 
        53, 50, 47, 44, 42, 40, 37, 35, 33, 31, 30, 28, 26, 25, 24, 22, 21, 20
    };

    // Notes on keypad
    const unsigned int pr2_preset5_notes[16] = {
        422, 597, 533, 474, 564, 502, 0, 398, 355, 317, 447, 0, 299, 0, 335, 376
    };

    typedef struct Song {
        char title[17];
        char artist[17];
        
        int c_notes; // Number of notes in song
        int* note_pr; // Note to load into PR2
        int* note_duration; // Time (ms) of note
        int* note_delta_t; // Time (ms) delay before note
    } Song;
    
    Song* rse_title;
    Song* dt_moon;
    Song* ssbu_title;
    
    Song* recorded_song;
    
    int len_songs = 3;
    Song** songs;
    
    void lcd_show_song(Song*); // Implemented in project4main.c
    
    /* ----------------------------------------------------------------------------- 
    **	init_songs
    **	Parameters:
    **		none
    **	Return Value:
    **		none
    **	Description:
    **		Allocate memory for Songs objects and populate them with their
    **      respective data.
    ** -------------------------------------------------------------------------- */
    void init_songs() {
        rse_title = malloc(sizeof(Song));
        dt_moon = malloc(sizeof(Song));
        ssbu_title = malloc(sizeof(Song));
        
        recorded_song = malloc(sizeof(Song));
        
        songs = malloc(len_songs * sizeof(Song*));
        songs[0] = rse_title;
        songs[1] = dt_moon;
        songs[2] = ssbu_title;
        
        strcpy(rse_title->title, "PKMN RSE Opening");
        strcpy(rse_title->artist, "Junichi Masuda");
        rse_title->c_notes = 160;
        
        int notes1[160] = {pr2_midi_preset[60], pr2_midi_preset[64], pr2_midi_preset[65], pr2_midi_preset[69], pr2_midi_preset[72], pr2_midi_preset[76], pr2_midi_preset[77], pr2_midi_preset[77], pr2_midi_preset[77], pr2_midi_preset[77], pr2_midi_preset[76], pr2_midi_preset[77], pr2_midi_preset[79], pr2_midi_preset[78], pr2_midi_preset[77], pr2_midi_preset[77], pr2_midi_preset[76], pr2_midi_preset[77], pr2_midi_preset[75], pr2_midi_preset[75], pr2_midi_preset[75], pr2_midi_preset[75], pr2_midi_preset[74], pr2_midi_preset[75], pr2_midi_preset[75], pr2_midi_preset[75], pr2_midi_preset[76], pr2_midi_preset[64], pr2_midi_preset[65], pr2_midi_preset[69], pr2_midi_preset[73], pr2_midi_preset[72], pr2_midi_preset[73], pr2_midi_preset[74], pr2_midi_preset[75], pr2_midi_preset[74], pr2_midi_preset[73], pr2_midi_preset[72], pr2_midi_preset[70], pr2_midi_preset[70], pr2_midi_preset[70], pr2_midi_preset[70], pr2_midi_preset[70], pr2_midi_preset[75], pr2_midi_preset[70], pr2_midi_preset[75], pr2_midi_preset[80], pr2_midi_preset[76], pr2_midi_preset[71], pr2_midi_preset[72], pr2_midi_preset[71], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[74], pr2_midi_preset[72], pr2_midi_preset[70], pr2_midi_preset[71], pr2_midi_preset[70], pr2_midi_preset[69], pr2_midi_preset[70], pr2_midi_preset[71], pr2_midi_preset[72], pr2_midi_preset[60], pr2_midi_preset[65], pr2_midi_preset[69], pr2_midi_preset[71], pr2_midi_preset[72], pr2_midi_preset[73], pr2_midi_preset[74], pr2_midi_preset[75], pr2_midi_preset[74], pr2_midi_preset[73], pr2_midi_preset[72], pr2_midi_preset[75], pr2_midi_preset[75], pr2_midi_preset[75], pr2_midi_preset[75], pr2_midi_preset[79], pr2_midi_preset[75], pr2_midi_preset[79], pr2_midi_preset[79], pr2_midi_preset[65], pr2_midi_preset[64], pr2_midi_preset[63], pr2_midi_preset[62], pr2_midi_preset[60], pr2_midi_preset[58], pr2_midi_preset[65], pr2_midi_preset[60], pr2_midi_preset[69], pr2_midi_preset[71], pr2_midi_preset[72], pr2_midi_preset[75], pr2_midi_preset[74], pr2_midi_preset[75], pr2_midi_preset[77], pr2_midi_preset[81], pr2_midi_preset[80], pr2_midi_preset[78], pr2_midi_preset[77], pr2_midi_preset[72], pr2_midi_preset[68], pr2_midi_preset[72], pr2_midi_preset[73], pr2_midi_preset[74], pr2_midi_preset[70], pr2_midi_preset[77], pr2_midi_preset[78], pr2_midi_preset[79], pr2_midi_preset[78], pr2_midi_preset[77], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[59], pr2_midi_preset[60], pr2_midi_preset[43], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[60], pr2_midi_preset[77], pr2_midi_preset[77], pr2_midi_preset[78], pr2_midi_preset[79], pr2_midi_preset[43], pr2_midi_preset[78], pr2_midi_preset[77], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[72], pr2_midi_preset[84], pr2_midi_preset[84], pr2_midi_preset[84], pr2_midi_preset[84], pr2_midi_preset[84], pr2_midi_preset[84], pr2_midi_preset[84], pr2_midi_preset[84], pr2_midi_preset[84], pr2_midi_preset[82], pr2_midi_preset[82], pr2_midi_preset[83]};
        int* rse_notes = malloc(160 * sizeof(int));
        memcpy(rse_notes, notes1, 160 * sizeof(int));
        
        int len1[160] = {82, 82, 82, 82, 82, 82, 1498, 82, 82, 1498, 82, 82, 30, 
        123, 1373, 82, 82, 82, 248, 123, 82, 40, 40, 40, 40, 40, 82, 82, 832, 
        123, 40, 1832, 82, 82, 665, 165, 165, 1998, 498, 61, 40, 40, 165, 165, 
        165, 165, 165, 165, 165, 165, 82, 82, 165, 82, 82, 165, 82, 82, 498, 
        98, 248, 315, 332, 30, 1998, 498, 165, 332, 30, 1832, 82, 82, 665, 165, 
        165, 1998, 82, 82, 123, 40, 82, 82, 123, 82, 248, 40, 40, 40, 40, 40, 
        165, 82, 165, 30, 1998, 373, 373, 248, 748, 40, 165, 40, 1957, 832, 
        2165, 498, 498, 832, 2165, 498, 498, 165, 40, 40, 40, 40, 40, 82, 82, 
        82, 82, 82, 82, 82, 123, 40, 40, 40, 82, 82, 82, 332, 123, 82, 165, 
        248, 40, 40, 40, 40, 40, 82, 82, 82, 82, 82, 82, 82, 123, 40, 40, 40, 
        82, 82, 82, 82, 82, 82, 82, 82};
        int* rse_len = malloc(160 * sizeof(int));
        memcpy(rse_len, len1, 160 * sizeof(int));
        
        int dt1[160] = {0, 83, 83, 84, 83, 83, 84, 1666, 167, 167, 1666, 167, 
        135, 32, 125, 1541, 167, 83, 84, 500, 333, 167, 166, 167, 167, 166, 
        167, 83, 84, 833, 125, 42, 1833, 83, 84, 666, 167, 167, 2000, 500, 
        166, 84, 83, 167, 166, 167, 167, 166, 167, 1167, 166, 167, 167, 166, 
        167, 167, 166, 167, 167, 500, 100, 250, 316, 302, 32, 4000, 500, 166, 
        302, 32, 1833, 83, 84, 666, 167, 167, 2000, 166, 334, 333, 167, 166, 
        334, 333, 167, 250, 41, 42, 42, 41, 84, 166, 167, 135, 32, 2000, 375, 
        375, 250, 791, 42, 167, 41, 2959, 833, 2167, 500, 500, 833, 2167, 500, 
        500, 166, 42, 42, 83, 83, 84, 166, 167, 333, 167, 333, 167, 167, 250, 
        83, 83, 84, 166, 167, 333, 334, 333, 167, 0, 166, 42, 42, 83, 83, 84, 
        166, 167, 333, 167, 333, 167, 167, 250, 83, 83, 84, 166, 167, 333, 167, 
        167, 166, 167};
        int* rse_dt = malloc(160 * sizeof(int));
        memcpy(rse_dt, dt1, 160 * sizeof(int));
        
        rse_title->note_pr = rse_notes;
        rse_title->note_duration = rse_len;
        rse_title->note_delta_t = rse_dt;
        
        int notes2[271] = {pr2_midi_preset[78], pr2_midi_preset[66], pr2_midi_preset[73], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[73], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[83], pr2_midi_preset[73], pr2_midi_preset[83], pr2_midi_preset[82], pr2_midi_preset[73], pr2_midi_preset[82], pr2_midi_preset[80], pr2_midi_preset[78], pr2_midi_preset[73], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[83], pr2_midi_preset[83], pr2_midi_preset[82], pr2_midi_preset[82], pr2_midi_preset[80], pr2_midi_preset[78], pr2_midi_preset[85], pr2_midi_preset[58], pr2_midi_preset[78], pr2_midi_preset[90], pr2_midi_preset[92], pr2_midi_preset[90], pr2_midi_preset[97], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[70], pr2_midi_preset[71], pr2_midi_preset[73], pr2_midi_preset[73], pr2_midi_preset[78], pr2_midi_preset[77], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[51], pr2_midi_preset[82], pr2_midi_preset[78], pr2_midi_preset[49], pr2_midi_preset[77], pr2_midi_preset[78], pr2_midi_preset[85], pr2_midi_preset[47], pr2_midi_preset[47], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[78], pr2_midi_preset[77], pr2_midi_preset[70], pr2_midi_preset[68], pr2_midi_preset[66], pr2_midi_preset[73], pr2_midi_preset[78], pr2_midi_preset[52], pr2_midi_preset[80], pr2_midi_preset[52], pr2_midi_preset[83], pr2_midi_preset[51], pr2_midi_preset[51], pr2_midi_preset[82], pr2_midi_preset[82], pr2_midi_preset[85], pr2_midi_preset[50], pr2_midi_preset[83], pr2_midi_preset[50], pr2_midi_preset[82], pr2_midi_preset[83], pr2_midi_preset[82], pr2_midi_preset[78], pr2_midi_preset[73], pr2_midi_preset[80], pr2_midi_preset[78], pr2_midi_preset[77], pr2_midi_preset[75], pr2_midi_preset[77], pr2_midi_preset[49], pr2_midi_preset[49], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[80], pr2_midi_preset[76], pr2_midi_preset[76], pr2_midi_preset[73], pr2_midi_preset[76], pr2_midi_preset[71], pr2_midi_preset[73], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[73], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[80], pr2_midi_preset[81], pr2_midi_preset[76], pr2_midi_preset[76], pr2_midi_preset[76], pr2_midi_preset[81], pr2_midi_preset[83], pr2_midi_preset[47], pr2_midi_preset[47], pr2_midi_preset[81], pr2_midi_preset[85], pr2_midi_preset[85], pr2_midi_preset[85], pr2_midi_preset[85], pr2_midi_preset[66], pr2_midi_preset[73], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[73], pr2_midi_preset[80], pr2_midi_preset[83], pr2_midi_preset[73], pr2_midi_preset[82], pr2_midi_preset[80], pr2_midi_preset[78], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[54], pr2_midi_preset[70], pr2_midi_preset[71], pr2_midi_preset[73], pr2_midi_preset[73], pr2_midi_preset[53], pr2_midi_preset[78], pr2_midi_preset[53], pr2_midi_preset[77], pr2_midi_preset[53], pr2_midi_preset[78], pr2_midi_preset[53], pr2_midi_preset[80], pr2_midi_preset[51], pr2_midi_preset[82], pr2_midi_preset[78], pr2_midi_preset[49], pr2_midi_preset[77], pr2_midi_preset[78], pr2_midi_preset[85], pr2_midi_preset[47], pr2_midi_preset[47], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[48], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[78], pr2_midi_preset[77], pr2_midi_preset[70], pr2_midi_preset[68], pr2_midi_preset[66], pr2_midi_preset[73], pr2_midi_preset[78], pr2_midi_preset[52], pr2_midi_preset[80], pr2_midi_preset[52], pr2_midi_preset[83], pr2_midi_preset[51], pr2_midi_preset[51], pr2_midi_preset[82], pr2_midi_preset[82], pr2_midi_preset[85], pr2_midi_preset[50], pr2_midi_preset[83], pr2_midi_preset[50], pr2_midi_preset[82], pr2_midi_preset[83], pr2_midi_preset[82], pr2_midi_preset[78], pr2_midi_preset[73], pr2_midi_preset[80], pr2_midi_preset[78], pr2_midi_preset[77], pr2_midi_preset[75], pr2_midi_preset[77], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[42], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[80], pr2_midi_preset[76], pr2_midi_preset[76], pr2_midi_preset[73], pr2_midi_preset[76], pr2_midi_preset[71], pr2_midi_preset[73], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[73], pr2_midi_preset[78], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[80], pr2_midi_preset[81], pr2_midi_preset[76], pr2_midi_preset[76], pr2_midi_preset[76], pr2_midi_preset[81], pr2_midi_preset[83], pr2_midi_preset[81], pr2_midi_preset[85], pr2_midi_preset[85], pr2_midi_preset[85], pr2_midi_preset[85], pr2_midi_preset[78], pr2_midi_preset[66], pr2_midi_preset[73], pr2_midi_preset[78], pr2_midi_preset[80], pr2_midi_preset[73]};
        int* moon_notes = malloc(271 * sizeof(int));
        memcpy(moon_notes, notes2, 271 * sizeof(int));
        
        int len2[271] = {153, 153, 153, 153, 153, 153, 153, 153, 153, 153, 153, 
        153, 153, 153, 153, 153, 1547, 308, 308, 308, 618, 161, 1082, 308, 308, 
        927, 463, 927, 2476, 308, 153, 153, 153, 153, 153, 153, 153, 153, 153, 
        153, 153, 153, 153, 153, 153, 153, 153, 153, 153, 153, 153, 153, 153, 
        153, 463, 308, 308, 308, 153, 153, 153, 1547, 153, 153, 153, 153, 153, 
        153, 153, 1547, 153, 153, 773, 153, 153, 1420, 780, 153, 153, 773, 153, 
        153, 153, 153, 153, 153, 153, 153, 1237, 153, 153, 153, 153, 153, 153, 
        153, 308, 618, 153, 153, 153, 153, 153, 153, 153, 243, 463, 308, 153, 
        153, 153, 153, 153, 153, 153, 153, 153, 243, 153, 161, 308, 308, 308, 
        153, 153, 50, 50, 50, 308, 153, 153, 161, 1392, 153, 243, 153, 161, 308, 
        308, 308, 153, 153, 1082, 153, 153, 153, 927, 153, 153, 2631, 153, 161, 
        153, 153, 161, 153, 324, 153, 153, 153, 153, 153, 153, 153, 153, 153, 
        153, 153, 308, 153, 153, 153, 153, 153, 153, 153, 153, 153, 153, 1547, 
        153, 153, 153, 153, 153, 153, 153, 1392, 153, 153, 153, 773, 153, 153, 
        1420, 780, 153, 153, 773, 153, 153, 153, 153, 153, 153, 153, 153, 1237, 
        153, 153, 153, 153, 153, 153, 153, 308, 618, 153, 153, 153, 153, 463, 
        243, 463, 308, 153, 153, 153, 153, 153, 153, 153, 153, 153, 243, 153, 
        161, 308, 308, 308, 153, 153, 50, 50, 50, 308, 153, 153, 161, 1392, 153, 
        243, 153, 161, 308, 308, 308, 153, 153, 1082, 153, 927, 153, 153, 2631, 
        463, 153, 161, 153, 153, 161};
        int* moon_len = malloc(271 * sizeof(int));
        memcpy(moon_len, len2, 271 * sizeof(int));
        
        int dt2[271] = {0, 163, 163, 163, 163, 163, 163, 163, 163, 163, 163, 
        163, 163, 163, 163, 163, 163, 1631, 326, 326, 326, 652, 163, 1141, 327, 
        326, 489, 489, 978, 2609, 326, 163, 326, 163, 163, 163, 163, 163, 163, 
        163, 163, 163, 163, 164, 163, 163, 163, 163, 163, 163, 163, 163, 163, 
        163, 163, 489, 326, 326, 326, 163, 163, 164, 1630, 163, 163, 163, 163, 
        163, 163, 163, 1631, 163, 163, 815, 163, 163, 1499, 822, 163, 163, 816, 
        163, 163, 163, 163, 163, 163, 163, 163, 1304, 163, 163, 163, 163, 163, 
        164, 163, 326, 652, 163, 163, 163, 163, 163, 163, 163, 489, 489, 327, 
        163, 163, 163, 163, 163, 163, 163, 163, 163, 489, 163, 326, 326, 326, 
        326, 163, 164, 54, 54, 55, 326, 163, 326, 326, 1467, 163, 489, 163, 327, 
        326, 326, 326, 163, 163, 815, 163, 163, 163, 979, 163, 163, 326, 163, 
        163, 163, 163, 326, 163, 652, 163, 163, 163, 163, 163, 163, 163, 164, 
        163, 163, 163, 326, 163, 163, 163, 163, 163, 163, 163, 163, 163, 163, 
        1631, 163, 163, 163, 163, 163, 163, 163, 1467, 163, 163, 163, 816, 163, 
        163, 1498, 823, 163, 163, 815, 163, 163, 163, 163, 163, 163, 164, 163, 
        1304, 163, 163, 163, 163, 163, 163, 163, 326, 653, 163, 163, 163, 163, 
        489, 489, 489, 326, 163, 163, 163, 163, 163, 163, 163, 164, 163, 489, 
        163, 326, 326, 326, 326, 163, 163, 55, 54, 54, 326, 163, 326, 327, 1467, 
        163, 489, 163, 326, 326, 326, 326, 164, 163, 1141, 163, 978, 163, 163, 
        163, 163, 163, 163, 163, 164};
        int* moon_dt = malloc(271 * sizeof(int));
        memcpy(moon_dt, dt2, 271 * sizeof(int));
        
        strcpy(dt_moon->title, "To the Moon");
        strcpy(dt_moon->artist, "Y. Sakaguchi");
        dt_moon->c_notes = 271;
        dt_moon->note_pr = moon_notes;
        dt_moon->note_duration = moon_len;
        dt_moon->note_delta_t = moon_dt;
        
        int notes3[89] = {pr2_midi_preset[64], pr2_midi_preset[66], pr2_midi_preset[67], pr2_midi_preset[64], pr2_midi_preset[67], pr2_midi_preset[74], pr2_midi_preset[73], pr2_midi_preset[71], pr2_midi_preset[73], pr2_midi_preset[76], pr2_midi_preset[72], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[66], pr2_midi_preset[69], pr2_midi_preset[74], pr2_midi_preset[76], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[66], pr2_midi_preset[64], pr2_midi_preset[66], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[66], pr2_midi_preset[64], pr2_midi_preset[62], pr2_midi_preset[64], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[66], pr2_midi_preset[64], pr2_midi_preset[66], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[66], pr2_midi_preset[62], pr2_midi_preset[64], pr2_midi_preset[64], pr2_midi_preset[66], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[65], pr2_midi_preset[71], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[66], pr2_midi_preset[64], pr2_midi_preset[66], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[66], pr2_midi_preset[64], pr2_midi_preset[62], pr2_midi_preset[64], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[66], pr2_midi_preset[64], pr2_midi_preset[66], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[71], pr2_midi_preset[69], pr2_midi_preset[66], pr2_midi_preset[62], pr2_midi_preset[64], pr2_midi_preset[64], pr2_midi_preset[66], pr2_midi_preset[67], pr2_midi_preset[69], pr2_midi_preset[65], pr2_midi_preset[71]};
        int* ssbu_notes = malloc(89 * sizeof(int));
        memcpy(ssbu_notes, notes3, 89 * sizeof(int));
        
        int len3[89] = {332, 332, 443, 221, 221, 221, 332, 332, 221, 887, 332, 
        332, 443, 221, 221, 221, 332, 332, 221, 332, 332, 221, 3554, 443, 443, 
        665, 54, 54, 332, 332, 221, 443, 443, 443, 332, 332, 221, 1776, 443, 
        443, 665, 54, 54, 332, 332, 221, 332, 332, 221, 887, 332, 332, 221, 
        443, 443, 887, 443, 443, 665, 54, 54, 332, 332, 221, 443, 443, 443, 
        332, 332, 221, 1776, 443, 443, 665, 54, 54, 332, 332, 221, 332, 332, 
        221, 887, 332, 332, 221, 443, 443, 887};
        int* ssbu_len = malloc(89 * sizeof(int));
        memcpy(ssbu_len, len3, 89 * sizeof(int));
        
        int dt3[89] = {0, 333, 333, 445, 222, 222, 222, 334, 333, 222, 889, 333, 
        334, 444, 222, 223, 222, 333, 333, 223, 333, 333, 223, 3555, 445, 444, 
        667, 111, 111, 333, 334, 222, 889, 444, 445, 333, 333, 222, 1778, 445, 
        444, 667, 111, 111, 333, 334, 222, 333, 334, 222, 889, 333, 333, 223, 
        444, 444, 889, 445, 444, 667, 111, 111, 333, 334, 222, 889, 444, 445, 
        333, 333, 223, 1777, 445, 444, 667, 111, 111, 334, 333, 222, 333, 334, 
        222, 889, 333, 334, 222, 444, 445};
        int* ssbu_dt = malloc(89 * sizeof(int));
        memcpy(ssbu_dt, dt3, 89 * sizeof(int));
        
        strcpy(ssbu_title->title, "SSBU Menu Screen");
        strcpy(ssbu_title->artist, "J. Nakatsuru");
        ssbu_title->c_notes = 89;
        ssbu_title->note_pr = ssbu_notes;
        ssbu_title->note_duration = ssbu_len;
        ssbu_title->note_delta_t = ssbu_dt;
        
        strcpy(recorded_song->title, "Recorded Song");
        strcpy(recorded_song->artist, "Guest");
        recorded_song->c_notes = 50;
        recorded_song->note_pr = malloc(50 * sizeof(int));
        recorded_song->note_duration = malloc(50 * sizeof(int));
        recorded_song->note_delta_t = malloc(50 * sizeof(int));
        
        int i = 0;
        for (; i < 50; i++) {
            recorded_song->note_pr[i] = 0;
            recorded_song->note_duration[i] = 0;
            recorded_song->note_delta_t[i] = 0;
        }
        
    }

#endif // _SONGS_HEADER
